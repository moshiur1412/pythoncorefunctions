import os
import csv
import cv2
import json
import tweepy
import requests
import cvlib as cv
from PIL import Image
from skimage import io
from io import BytesIO

def image_info(url):
	if '.jpg' not in url:
		response = requests.get(url)
		get_im = Image.open(BytesIO(response.content))
		rgb_im = get_im.convert('RGB')
		rgb_im.save('storage/png_to_jpg.jpg')
		im = cv2.imread('storage/png_to_jpg.jpg')
		conf = cv.detect_common_objects(im)
		os.remove('storage/png_to_jpg.jpg')
	else:
		im = io.imread(url)
		conf = cv.detect_common_objects(im)
	info = [url, conf]
	return info

with open("credentials.json", "r") as file:
	creds = json.load(file)

auth = tweepy.OAuthHandler(creds['consumer_key'], creds['consumer_secret'])
auth.set_access_token(creds['access_token'], creds['access_secret'])

api = tweepy.API(auth)

input_users = input ("Enter number of users: ")
input_keyword = input ("Enter keyword: ")
input_tweets = input ("Enter number of tweets: ")
data = {}
for index, search in enumerate(api.search(q=input_keyword,  lang='ja', count=input_users)):
		img_info = {'profile_image' : image_info(search.user.profile_image_url.replace("_normal", ""))}
		stuff = api.user_timeline(screen_name = search.user.screen_name, count = input_tweets, include_rts = True)
		for status in stuff:
			for media in status.entities.get("media",[{}]):
				if media.get("type", None) == "photo":
					conf = image_info(media["media_url"])
					img_info[search.id_str] = conf
					data[search.user.screen_name] = img_info

try:
    with open("storage/image_labels.json", "a") as jsonfile:
    	jsonfile.write(json.dumps(data))
    	jsonfile.write("\n")
except (IOError, ValueError) as error:
	with open("storage/image_labels.json", "w+") as jsonfile:
		jsonfile.write(json.dumps(data))
print('done!')