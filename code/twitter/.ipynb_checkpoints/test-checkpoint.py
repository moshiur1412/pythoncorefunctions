import csv
import json
import time
import tweepy
from tweepy import Stream
from tweepy import OAuthHandler
from tweepy.streaming import StreamListener

with open("credentials.json", "r") as file:
	cred_data = json.load(file)
	creds = cred_data['twitter']

auth = OAuthHandler(creds['consumer_key'], creds['consumer_secret'])
auth.set_access_token(creds['access_token'], creds['access_secret'])

api = tweepy.API(auth)

with open('users.csv','rt')as f:
	data = csv.reader(f)
	for row in data:
		screen_name = row[0].split("https://twitter.com/",1)[1]
		writer = csv.writer(open("crawled_tweets.csv", 'a'))
		for tweet in tweepy.Cursor(api.user_timeline, id=screen_name).items():
			print ('{0}:'.format(tweet.created_at))
			print (tweet.text)
			writer.writerow([creds['website']+tweet.user.screen_name+'/status/'+tweet.id_str, tweet.text, tweet.created_at])