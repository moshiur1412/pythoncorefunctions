import csv
import json
import time
import tweepy

from tweepy import Stream
from tweepy import OAuthHandler
from tweepy.streaming import StreamListener

with open("credentials.json", "r") as file:
	creds = json.load(file)

auth = OAuthHandler(creds['consumer_key'], creds['consumer_secret'])
auth.set_access_token(creds['access_token'], creds['access_secret'])

api = tweepy.API(auth)

input_total = input ("Enter the number of collected posts: ")
input_keyword = input("Enter the Keyword: ")

i= 1
start = time.time()

writer = csv.writer(open("tweets.csv", "a"))

for status in api.search(q=input_keyword, lang='ja', result_type='recent', count=input_total):
	print('{0}:'.format(status.created_at))
	print(status.text)
	writer.writerow(['https://twitter.com/'+status.user.screen_name+'/status/'+status.id_str, status.text, status.created_at])
	i = i + 1

elapsed_time = time.time() - start
print ("elapsed_time:{0}".format(elapsed_time) + "[sec")

