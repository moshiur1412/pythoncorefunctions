import json
import tweepy
import csv

with open("credentials.json", "r") as file:
	creds = json.load(file)

auth = tweepy.OAuthHandler(creds['consumer_key'], creds['consumer_secret'])
auth.set_access_token(creds['access_token'], creds['access_secret'])

api = tweepy.API(auth)

input_total = input ("Enter number of users: ")
input_keyword = input("Enter your all tags here: ")
input_status = input("Enter number of minimum statuses: ")

writer = csv.writer(open("tweets_sample.csv", "a"))

for index, search in enumerate(api.search(q=input_keyword,  lang='ja', count=input_total)):
	print ("%d: %s" % (index+1, search.user.name))
	
	if search.user.statuses_count >= int(input_status):
		writer.writerow(['https://twitter.com/'+search.user.screen_name])
	


